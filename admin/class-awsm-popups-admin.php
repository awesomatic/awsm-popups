<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       awesomatic.nl
 * @since      0.5
 *
 * @package    Awsm_Popups
 * @subpackage Awsm_Popups/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Awsm_Popups
 * @subpackage Awsm_Popups/admin
 * @author     Jordi Radstake <jordi@awesomatic.nl>
 */
class Awsm_Popups_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    0.5
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    0.5
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    0.5
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    0.5
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Awsm_Popups_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Awsm_Popups_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/awsm-popups-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    0.5
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Awsm_Popups_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Awsm_Popups_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/awsm-popups-admin.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * Register the post type for the admin area.
	 *
	 * @since    0.7
	 */
	public function register_custom_post_types() {

		/**
		 * This function let you create and edit new pop-ups in the admin area.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Awsm_Popups_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Awsm_Popups_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		$labels = array(
			'name'               => __( 'Pop-ups' ),
			'singular_name'      => __( 'Pop-up' ),
			'add_new'            => __( 'Nieuwe pop-up' ),
			'add_new_item'       => __( 'Nieuwe pop-up' ),
			'edit_item'          => __( 'Edit Pop-up' ),
			'new_item'           => __( 'Nieuwe Pop-up', 'awsm-popups' ),
			'all_items'          => __( 'Alle pop-ups', 'awsm-popups' ),
			'view_item'          => __( 'Bekijk Pop-up' ),
			'search_items'       => __( 'Zoek Pop-ups' ),
			'featured_image'     => 'Img',
			'set_featured_image' =>	'Voeg afbeelding toe'
		);
		
		// The arguments for our post type, to be entered as parameter 2 of register_post_type()
		$args = array(
			'labels'            	=> $labels,
		
			// The only way to read that field is using this code: $obj = get_post_type_object( 'your_post_type_name' ); echo esc_html( $obj->description );
			//'description'       => 'Type some description ..',
		
			// Whether to exclude posts with this post type from front end search results
			'public'            	=> true,
		
			// Check out all available icons: https://developer.wordpress.org/resource/dashicons/#layout
			'menu_icon'         	=> 'dashicons-megaphone',
		
			// Options: 26 Comments 2 - Dashboard 4 - Seperator 5 - Posts 10 - Media 15 - Links 20 - Pages - 25 Comments - 
			// 59 Seperator - 60 Appaerence - 65 Plugins - 70 Users - 75 Tools - 80 Settings - 99 - Seperator - 200 Seperator - 205 Starter Theme - 220 Popups - 300 Seperator
			'menu_position'     	=> 220, 
		
			// 'comments' editor has to be true to make us of the Gutenberg editor ,'excerpt'
			'supports'          	=> array( 'title', 'editor', 'thumbnail', 'custom-fields' ),
		
			'rewrite'            	=> array( 
				/* 
				 * 'popups' Will be used as slug on the permalink 
				 */
				'slug'              => 'popups', // Custom string. Just be aware of potential conflicts with other plugins or themes using the same slug.
				/* 
				 * Example: if your permalink structure is /blog/, 
				 * then your links will be: false->/news/, true->/blog/news/.
				 */
				'with_front'        => false // Defaults to |true|
			),
		
			//
			//'taxonomies' => array('popup-category'),
		
			//
			'has_archive'       	=> false, // Set to false hides Archive Pages
		
			'publicly_queryable' 	=> false, // Set to false hides Single Pages
		
			// Has to be true to make use of the the Gutenberg editor
			'show_in_rest'      	=> true,
		
			//
			'show_in_admin_bar' 	=> false,
		
			//
			'show_in_nav_menus' 	=> false,
		
			//
			'has_archive'       	=> false,
		
			// A URL slug for your taxonomy
			'query_var'         	=> false
			
		);
		
		// Call the actual WordPress function
		register_post_type( 'awsm-popup', $args);

		// register_taxonomy(

		// 	// Give it a name, it will be used as slug as well
		// 	'awsm-popup-category',
		
		// 	// Connect it to the custom 'popup' type
		// 	'awsm-popup',
		
		// 	//
		// 	array(
		// 	'label' => __( 'Categories' ),
		// 	'rewrite' => array( 'slug' => 'popup-category' ),
		// 	'hierarchical' => true,
		// 	)
		
		// );

	}	

	/**
	 * Add options page for Advanced Custom Fields.
	 *
	 * @since    0.5
	 */
	public function add_options_page() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Awsm_Popups_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Awsm_Popups_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */	 

		if (function_exists('acf_add_options_sub_page')) {

			acf_add_options_sub_page(array(

				'page_title' 	=> 'Instellingen voor Popups',
				'menu_title'	=> 'Instellingen',
				'parent_slug'	=> 'edit.php?post_type=awsm-popup',

			));

		}

		/**
		 * ACF EXPORT: ADD LOCAL FIELD GROUP
		 *
		 * Reference: 
		 */

		if( function_exists('acf_add_local_field_group') ):

			acf_add_local_field_group(array(
				'key' => 'group_5e6927f92a043',
				'title' => 'Cookies',
				'fields' => array(
					array(
						'key' => 'field_5e692810fc376',
						'label' => 'Buffer',
						'name' => 'awsm_popups_cookie_buffer',
						'type' => 'number',
						'instructions' => 'Na hoeveel tijd moeten pop-ups verschijnen?',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => 5,
						'placeholder' => 5,
						'prepend' => '',
						'append' => 'seconden',
						'min' => 1,
						'max' => '',
						'step' => 1,
					),
					array(
						'key' => 'field_5e7cc52b030ff',
						'label' => 'Time-out',
						'name' => 'awsm_popups_cookie_timeout',
						'type' => 'number',
						'instructions' => 'Hoeveel dagen moeten pop-ups verborgen worden nadat een bezoeker deze heeft weggeklikt?',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => 1,
						'placeholder' => 1,
						'prepend' => '',
						'append' => 'dagen',
						'min' => 1,
						'max' => '',
						'step' => 1,
					),
				),
				'location' => array(
					array(
						array(
							'param' => 'options_page',
							'operator' => '==',
							'value' => 'acf-options-instellingen',
						),
					),
				),
				'menu_order' => 0,
				'position' => 'side',
				'style' => 'default',
				'label_placement' => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen' => '',
				'active' => true,
				'description' => '',
			));
			
			acf_add_local_field_group(array(
				'key' => 'group_5e74b0261d546',
				'title' => 'Externe content',
				'fields' => array(
					array(
						'key' => 'field_5e74b03eb500e',
						'label' => 'Embed code',
						'name' => 'awsm_popups_embed_externe_scripts',
						'type' => 'textarea',
						'instructions' => 'Embed een filmpje, audio-bestand, foto, formulier e.d. van een externe website.',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'maxlength' => '',
						'rows' => '',
						'new_lines' => '',
					),
				),
				'location' => array(
					array(
						array(
							'param' => 'post_type',
							'operator' => '==',
							'value' => 'awsm-popup',
						),
					),
				),
				'menu_order' => 0,
				'position' => 'normal',
				'style' => 'default',
				'label_placement' => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen' => '',
				'active' => true,
				'description' => '',
			));
			
			endif;

	}

}
