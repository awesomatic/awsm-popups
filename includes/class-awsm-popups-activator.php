<?php

/**
 * Fired during plugin activation
 *
 * @link       awesomatic.nl
 * @since      0.5
 *
 * @package    Awsm_Popups
 * @subpackage Awsm_Popups/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      0.5
 * @package    Awsm_Popups
 * @subpackage Awsm_Popups/includes
 * @author     Jordi Radstake <jordi@awesomatic.nl>
 */
class Awsm_Popups_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    0.5
	 */
	public static function activate() {

	}

}
