<?php

/**
 * Fired during plugin deactivation
 *
 * @link       awesomatic.nl
 * @since      0.5
 *
 * @package    Awsm_Popups
 * @subpackage Awsm_Popups/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      0.5
 * @package    Awsm_Popups
 * @subpackage Awsm_Popups/includes
 * @author     Jordi Radstake <jordi@awesomatic.nl>
 */
class Awsm_Popups_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    0.5
	 */
	public static function deactivate() {

	}

}
