<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       awesomatic.nl
 * @since      0.5
 *
 * @package    Awsm_Popups
 * @subpackage Awsm_Popups/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      0.5
 * @package    Awsm_Popups
 * @subpackage Awsm_Popups/includes
 * @author     Jordi Radstake <jordi@awesomatic.nl>
 */
class Awsm_Popups_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    0.5
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'awsm-popups',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
