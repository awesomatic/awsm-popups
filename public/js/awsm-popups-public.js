(function($) {
	'use strict';

  /**
   * 
	 */
  $(document).ready(function() {

    var currentTime = new Date().getTime();

    /**
     * Add hours.
     * 
     * @since   0.5
     */
    Date.prototype.addHours = function(h) {   

      /**
       * Add hours.
       * 
       * 
       */ 

      this.setTime(this.getTime() + (h*60*60*1000)); 
      return this;   
    }

    // Get time after 24 hours
    var after24hours = new Date().addHours(10).getTime();

    /**
     * Add days.
     * 
     * @doc https://stackoverflow.com/questions/54941882/set-timing-for-90-days-javascript
     * 
     * Create an Advanced Custom Field named awsm_popups_cookie_timeout.
     * 
     */

      Date.prototype.addDays = function(d) {    
        this.setDate(this.getDate() + d); 
        return this;   
      }

      //Get time after --- the_field('awsm_popups_cookie_timeout', 'option'); --- days
      var after90days = new Date().addDays(awsm_popups_cookie_timeout).getTime();
    
      var curr_date = new Date(currentTime);
      var after90days_date = new Date(after90days);

    /**
     * Click button to close pop-up.
     * 
     * @doc
     * 
     * 
     * 
     */

    $('.awsm-popup__btn--close').click(function() {
  
      $('.awsm-popup').addClass('awsm-popup--hidden');
      $('.awsm-popup__content').addClass('awsm-popup__content--hidden');

      //Set desired time till you want to hide that div
      localStorage.setItem('popupdesiredTime', after90days);

    });

    /**
     * Click outside pop-up to close pop-up. 
     * 
     * 
     */

    $(document).mouseup(function (e) { 
              
      if ($(e.target).closest(".awsm-popup__content").length === 0) { 
                 
        $('.awsm-popup').addClass('awsm-popup--hidden');
        
        //Set desired time till you want to hide that div
        localStorage.setItem('popupdesiredTime', after90days);
        
      } 

    });           

    /**
     * If desired time >= currentTime, based on that HIDE / SHOW
     * 
     * 
     */
    
    if (localStorage.getItem('popupdesiredTime') >= currentTime) {

      $('.awsm-popup').addClass('awsm-popup--hidden');

    } else { 
            
      setTimeout(function() {

        $('.awsm-popup').removeClass('awsm-popup--hidden');

      }, awsm_popups_time + zerozerozero);

    }

  });

  /**
   * clear data function
   * 
   * 
   */
  function clearClick(number) {
  	localStorage.clear();
  }

})(jQuery);
