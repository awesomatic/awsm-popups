<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       awesomatic.nl
 * @since      0.5
 *
 * @package    Awsm_Popups
 * @subpackage Awsm_Popups/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<div class="awsm-popup awsm-popup--hidden">
    <div class="awsm-popup__content">
        <div class="awsm-popup__content__left">
            <?php the_content(); ?>
            <?php the_field('awsm_popups_embed_externe_scripts'); ?>
        </div>
        
        <?php
        $thumb_id = get_post_thumbnail_id();
        $thumb_url = wp_get_attachment_image_src($thumb_id,'full', true);
        ?>
    
        
        <div class="awsm-popup__content__right" style="background-image: url('<?php echo $thumb_url[0]; ?>'); background-size:cover; background-position:center;">&nbsp;</div>
    
        <button class="awsm-popup__btn--close">X</button>

    </div>
</div>
